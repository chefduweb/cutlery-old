source $(dirname $0)/config.cfg

echo -e "\033[32m Nieuwe plugin installeren... "
echo -e "\033[0m"

#First parameter (optional. like the rest) is the name of the folder:
if [[ -z "$1" ]]; then
	printf "Wat is de map van het project?"
	read THEDIR
else
	THEDIR=$1
fi

#Second parameter is the plugin they want to install:
if [[ -z "$2" ]]; then
	printf "Welke plugin wil je installeren?"
	read THEPLUGIN
else
	THEPLUGIN=$2
fi


THEPLUGIN=$default_repo_prefix$THEPLUGIN

echo $THEPLUGIN;

cd ~
cd dropbox/Wonder\ local/
cd $THEDIR


echo -e "\033[32m Composer pakket aanmaken... "
echo -e "\033[0m"

composer config repositories.$THEPLUGIN vcs $default_repo_url/$THEPLUGIN.git
composer require $default_vendor/$THEPLUGIN:$default_version

echo -e "\033[32m Plugin binnenhalen... "
echo -e "\033[0m"

composer update


echo -e "\033[32m Plugin activeren... "
echo -e "\033[0m"

wp plugin activate $THEPLUGIN
