# [Cutlery](http://www.chefduweb.nl/open-source/cutlery)

Cutlery is a collection of Bash scripts that help you scaffold WordPress projects. They are based on the Canapé WordPress Stack and require Composer, WP CLI and (naturally) Git to be installed.

## Quick Start

Do git clone git@bitbucket.org:chefduweb/Cutlery.git and add some nice aliases to your .bash_profile like this:

 * cook-site=bash Cutlery/site.sh
 * cook-plugin=bash Cutlery/plugin.sh
 * etc.


## Features

* Automatic scaffolding of [Canapé](http://www.chefduweb.nl/open-source/canape) projects
* Fully configurable to your own setup
* Dependency management with [Composer](http://getcomposer.org)
* Automated deployments with [Capistrano](http://www.capistranorb.com/)
* Better folder structure
* Easy WordPress configuration with automated environment specific files
* Easily add custom plugins and repo's to your composer.json
 

Cutlery is meant as a base for you to fork and modify to fit your needs. It is delete-key friendly and you can strip out or modify any part of it. You'll also want to customize Cutlery with settings specific to your sites/company.

## Requirements

* Git
* Composer (globally)
* WP CLI (globally)
* PHP >= 5.3.2 (for Composer)
* Ruby >= 1.9 (for Capistrano)

Not deploying with Capistrano? Then don't worry about Ruby.


## Canapé

### Folder Structure

```
├── Capfile
├── composer.json
├── config
│   ├── application.php
│   ├── deploy
│   │   ├── staging.rb
│   │   └── production.rb
│   ├── deploy.rb
│   ├── environments
│   │   ├── development.php
│   │   ├── staging.php
│   │   └── production.php
│   └── application.php
├── Gemfile
├── vendor
└── web
    ├── app
    │   ├── mu-plugins
    │   ├── plugins
    │   └── themes
    ├── wp-config.php
    ├── index.php
    └── wp
```

The organization of Canapé is similar to putting WordPress in its own subdirectory but with some improvements.

* In order not to expose sensitive files in the webroot, Canapé moves what's required into a `web/` directory including the vendor'd `wp/` source, and the `wp-content` source.
* `wp-content` (or maybe just `content`) has been named `app` to better reflect its contents. It contains application code and not just "static content". It also matches up with other frameworks such as Symfony and Rails.
* `wp-config.php` remains in the `web/` because it's required by WP, but it only acts as a loader. The actual configuration files have been moved to `config/` for better separation.
* Capistrano configs are also located in `config/` to make it consistent.
* `vendor/` is where the Composer managed dependencies are installed to.
* `wp/` is where the WordPress core lives. It's also managed by Composer but can't be put under `vendor` due to WP limitations.


### Configuration Files

The root `web/wp-config.php` is required by WordPress and is only used to load the other main configs. Nothing else should be added to it.

`config/application.php` is the main config file that contains what `wp-config.php` usually would. Base options should be set in there.

For environment specific configuration, use the files under `config/environments`. By default there's is `development`, `staging`, and `production` but these can be whatever you require.

The environment configs are required **before** the main `application` config so anything in an environment config takes precedence over `application`.

Note: You can't re-define constants in PHP. So if you have a base setting in `application.php` and want to override it in `production.php` for example, you have a few options:

* Remove the base option and be sure to define it in every environment it's needed
* Only define the constant in `application.php` if it isn't already defined.

### Environment Variables

Canapé tries to separate config from code as much as possible and environment variables are used to achieve this. The benefit is there's a single place (`.env`) to keep settings like database or other 3rd party credentials that isn't committed to your repository.

[PHP dotenv](https://github.com/vlucas/phpdotenv) is used to load the `.env` file. All variables are then available in your app by `getenv`, `$_SERVER`, or `$_ENV`.

Currently, the following env vars are required:

* `DB_USER`
* `DB_NAME`
* `DB_PASSWORD`
* `WP_HOME`
* `WP_SITEURL`


## Composer

[Composer](http://getcomposer.org) is used to manage dependencies. Canapé considers any 3rd party library as a dependency including WordPress itself and any plugins.

See these two blogs for more extensive documentation:

* [Using Composer with WordPress](http://roots.io/using-composer-with-wordpress/)
* [WordPress Plugins with Composer](http://roots.io/wordpress-plugins-with-composer/)

## Plugins

### Public plugins

[WordPress Packagist](http://wpackagist.org/) is already registered in the `composer.json` file so any plugins from the [WordPress Plugin Directory](http://wordpress.org/plugins/) can easily be required.

These public plugins can be installed using `Cutlery/wp-plugin.sh [name-of-plugin]`, so the command `Cutlery/wp-plugin.sh advanced-custom-fields` would add ACF to your composer file as a required dependency. It will then update composer (thus downloading the plugin) and it will use WP CLI to automatically activate the plugin for you. 

### Private plugins

Private plugins require some configuration. In `config.cfg` you can edit the default Git repo and the default vendor namespace for Cutlery to use. If the repo-url is filled in the right way the command `Cutlery/plugin.sh [name-of-repo]` will automatically add your repo as a Composer dependancy. It will also update composer, which will download the repo in your project. Then it will use WP CLI to activate the plugin with the `[name-of-repo]` value.

#### Important notice: Your repo's will have to have composer.json files to work properly.


## WP-CLI

Cutlery works with [WP-CLI](http://wp-cli.org/) just like any other WordPress project would. Previously we required WP-CLI in our `composer.json` file as a dependency. This has been removed since WP-CLI now recommends installing it globally with a `phar` file. It also caused conflicts if you tried using a global install.

The `wp` command will automatically pick up Cutlery's subdirectory install as long as you run commands from within the project's directory (or deeper). Canapé includes a `wp-cli.yml` file that sets the `path` option to `web/wp`. Use this config file for any further [configuration](http://wp-cli.org/config/).



## Contributing

Everyone is welcome to help [contribute](CONTRIBUTING.md) and improve this project. There are several ways you can contribute:

* Suggesting new features
* Writing or refactoring code
* Fixing [issues](https://github.com/chefduweb/Cutlery/issues)

