source $(dirname $0)/config.cfg

echo -e "\033[32m Installing a new plugin... "
echo -e "\033[0m"

#First parameter (optional. like the rest) is the name of the folder:
if [[ -z "$1" ]]; then
	printf "What folder is the project in?"
	read THEDIR
else
	THEDIR=$1
fi

#Second parameter is the plugin they want to install:
if [[ -z "$2" ]]; then
	printf "Which plugin to install?"
	read THEPLUGIN
else
	THEPLUGIN=$2
fi


echo $THEPLUGIN;

cd ~
cd dropbox/Wonder\ local/
cd $THEDIR


echo -e "\033[32m Creating a Composer package... "
echo -e "\033[0m"

composer require wpackagist/$THEPLUGIN:$default_version

echo -e "\033[32m Downloading the plugin... "
echo -e "\033[0m"

composer update


echo -e "\033[32m Activating the plugin... "
echo -e "\033[0m"

wp plugin activate $THEPLUGIN
