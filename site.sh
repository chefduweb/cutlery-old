source $(dirname $0)/config.cfg

if [[ -z "$1" ]]; then
	printf "Wat is de (nieuw te maken) map van het project?"
	read THEDIR
else
	THEDIR=$1
fi


cd ~
cd dropbox/Wonder\ local/

echo -e "\033[32m Canapé installeren... "
echo -e "\033[0m"

mkdir $THEDIR
cd $THEDIR
git clone $default_repo_url/canape.git . && rm -rf .git


echo -e "\033[32m WordPress & Dependencies installeren... "
echo -e "\033[0m"

composer install


echo -e "\033[32m Thema installeren... "
echo -e "\033[0m"

cd web/app/themes/
mv carte-blanche $THEDIR
cd $THEDIR
rm -rf .git
cd ../../../../


echo -e "\033[32m Database connectie maken... "
echo -e "\033[0m"

cat .env.example | sed "s/{DIRNAME}/"$THEDIR"/" > .env

wp db create


echo -e "\033[32m WordPress installeren... "
echo -e "\033[0m"

printf "Wat is de titel van de site? "
read WPNAME

wp core install --url='$main_domain/$THEDIR/web' --title=$WPNAME --admin_name=$admin_name --admin_email=$admin_email --admin_password=$admin_pw


echo -e "\033[32m Gebruikers maken & Plugins activeren... "
echo -e "\033[0m"


#wp plugin activate cuisine
#wp plugin activate chef-forms
#wp theme activate $THEDIR


echo -e "\033[32m Overige instellingen... "
echo -e "\033[0m"

wp post update 2 --post_name=home --post_title=Home
wp option update page_on_front '2'
wp option update show_on_front 'page'
wp option update blog_public '0'
